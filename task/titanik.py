import pandas as pd


def get_titanic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    # Extract titles from the 'Name' column, considering both variations with and without a period
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+)\.?')

    # Filter the dataset for specific titles
    titles_to_filter = ["Mr", "Mrs", "Miss"]
    filtered_df = df[df['Title'].isin(titles_to_filter)]

    # Calculate the median age values for the specified titles
    median_ages = filtered_df.groupby('Title')['Age'].median().to_dict()

    # Count the number of missing values for the specified titles
    missing_counts = filtered_df.groupby('Title')['Age'].apply(lambda x: x.isnull().sum()).to_dict()

    # Create a list of tuples in the specified format
    result = [(title, missing_counts.get(title, 0), round(median_ages.get(title, 0))) for title in titles_to_filter]

    # Fill missing values in the 'Age' column with the respective median values
    for title, median_age in median_ages.items():
        df.loc[(df['Title'] == title) & (df['Age'].isnull()), 'Age'] = median_age

    return result
